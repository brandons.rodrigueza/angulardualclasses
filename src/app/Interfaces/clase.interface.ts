export interface Clase {
    titulo: string,
    descripcion: string,
    hora_inicio: string | null,
    url: string,
    id_clase: string,
    dia_clase?: string | null,
    hora_fin?:  string | null,
}
