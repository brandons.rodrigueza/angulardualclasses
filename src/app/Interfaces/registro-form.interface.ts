export interface registroForm {
    email: string;
    nombre: string;
    password: string;
    role: string;
    estado_perfil: string;
}