import { DashboardComponent } from './Pages/dashboard/dashboard.component';
import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import { LoginComponent } from './Auth/login/login.component';
import { RegistroComponent } from './Auth/registro/registro.component';
import { AuthGuard } from './Guards/auth.guard';
import { ClassroomComponent } from './Pages/classroom/classroom.component';
import { ProfileComponent } from './Pages/profile/profile.component';
import { MainComponent } from './Pages/main.component';
import { CalendarComponent } from './Pages/calendar/calendar.component';
import { ClassComponent } from './Pages/class/class.component';
import { AdminComponent } from './Pages/admin/admin.component';
import { ListEstAdminComponent } from './Pages/list-est-admin/list-est-admin.component';
import { ListProfAdminComponent } from './Pages/list-prof-admin/list-prof-admin.component';
import { CarroComprasComponent } from './Pages/carro-compras/carro-compras.component';

const routes: Routes = [
  
  { path: 'login', component: LoginComponent  },
  { path: 'registro', component: RegistroComponent }, 
  { path: '',
    component: MainComponent,
    children: [
      { path: 'dashboard', component: DashboardComponent},
      { path: 'classroom', component: ClassroomComponent},
      { path: 'admin', component: AdminComponent },
      { path: 'profile', component: ProfileComponent},
      { path: 'calendar', component: CalendarComponent},
      { path: 'class', component: ClassComponent},
      { path: 'list_est', component: ListEstAdminComponent},
      { path: 'list_prof', component: ListProfAdminComponent},
      { path: 'carro_compras', component: CarroComprasComponent},
      { path: '**', redirectTo:'/dashboard', pathMatch:'full' },
    ],
  canActivate: [AuthGuard]
  },
  // {path: '**', component: NopageComponent}
]

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
