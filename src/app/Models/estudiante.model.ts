export class Estudiante {
    constructor (
        public estado_perfil: string,
        public nombre: string,
        public email: string,
        public password: string,
        public role: boolean,
        public img?: string,
        public google?: string,
        public id?: string,
        public apellido?: string,
        public telefono?: string,
        public calificacion?: number,
        public perfil_linkedlin?: string,
        public resena?: string
    ) {}
}