export class ClaseModule {
  constructor(
    public titulo: string,
    public descripcion: string,
    public hora_inicio: string,
    public url: string,
    public id_clase: string,
    public hora_fin?: string,
    public tipo?: string,
    public cupor?: string,
    public id?: string
    ){

  }
 }
