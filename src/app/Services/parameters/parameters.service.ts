import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ParametersService {

  parametersData: any;

  constructor(private http: HttpClient) { }


  async loadParameters() {
    await this.http
      .get<any>('../../assets/config/app_parameters.json')
      .toPromise()
      .then((res: any) => {
        this.parametersData = res['data'].parameters;
      })
  }
}
