import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { environment } from '../../environments/environment';
import { LoginForm } from '../Interfaces/login-form.interface';
import { catchError, map, tap } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { Router } from '@angular/router';
import { UtilService } from './util.service';
import { Estudiante } from '../Models/estudiante.model';
import { Profesor } from '../Models/profesor.model';

const base_url = environment.base_url;

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  public Estudiante: Estudiante;
  public Profesor: Profesor;


  constructor(private http: HttpClient, private router:Router, private utilService: UtilService) { }

  get uid():string {
    return this.Estudiante.id || '';
  }

  get uidprof():string {
    return this.Profesor.id || '';
  }

  verificarToken():Observable<boolean>{

    const token = this.utilService.getLocalStorage('token') || '';

    return this.http.get(`${base_url}/login/renew`,{
      headers: {
        'x-token': token
      }
    }).pipe(
      tap((resp:any)=>{
        this.utilService.saveInLocalStorage('token',resp.token);
      }),
      map(resp => true),
      catchError(error => of(false))
    )
  }

  crearUsuario(formData: any) {
    return this.http.post(`${base_url}/registro`,formData);
  }

  login(formData: LoginForm) {
    return this.http.post(`${base_url}/login`,formData)
          .pipe(
            tap((resp: any) => {
              this.utilService.saveInLocalStorage('token',resp.token)
              const rol = resp.usuario.role;
              if(rol === 'Profesor') {
                const {
                  estado_perfil,
                  nombre,
                  email,
                  password,
                  role,
                  img,
                  google,
                  id,
                  apellido,
                  telefono,
                  calificacion,
                  perfil_linkedlin,
                  resena,documents,
                  documento_identidad,
                  categoria,
                  materia,
                  titulo_prof,
                  numero_nequi,
                  numero_daviplata,
                  banco,
                  numero_cuenta,
                  tipo_cuenta,
                  precio_hora,
                  tipo_identificacion
                } = resp.usuario;
                this.Profesor = new Profesor(
                  estado_perfil,
                  nombre,
                  email,
                  password,
                  role,
                  img,
                  google,
                  id,
                  apellido,
                  telefono,
                  calificacion,
                  perfil_linkedlin,
                  resena,
                  documents,
                  documento_identidad,
                  categoria,
                  materia,
                  titulo_prof,
                  numero_nequi,
                  numero_daviplata,
                  banco,
                  numero_cuenta,
                  tipo_cuenta,
                  precio_hora,
                  tipo_identificacion
                  );
                this.utilService.saveInLocalStorage('userConnect', JSON.stringify(this.Profesor));
                this.utilService.setUserConnect(this.Profesor);
              } else {
                const {estado_perfil,nombre,email,password,role,img,google,id,apellido,telefono,calificacion,perfil_linkedlin,resena} = resp.usuario
                this.Estudiante = new Estudiante(estado_perfil,nombre,email,password,role,img,google,id,apellido,telefono,calificacion,perfil_linkedlin,resena);
                this.utilService.saveInLocalStorage('userConnect', JSON.stringify(this.Estudiante));
                this.utilService.setUserConnect(this.Estudiante);
              }
              this.utilService.saveInLocalStorage('userRole',rol.toLowerCase());
            })
          )
  }

  loginGoogle(token: any) {
    return this.http.post(`${base_url}/login/google`,{token})
          .pipe(
            tap((resp: any) => {
              this.utilService.saveInLocalStorage('token',resp.token);
              const rol = resp.usuario.role;
              if(rol === 'Profesor') {
                const {estado_perfil,nombre,email,password,role,img,google,id,apellido,telefono,calificacion,perfil_linkedlin,resena,documents,documento_identidad,categoria,materia,titulo_prof,numero_nequi,numero_daviplata,banco,numero_cuenta,tipo_cuenta,precio_hora,tipo_identificacion} = resp.usuario;
                this.Profesor = new Profesor(estado_perfil,nombre,email,password,role,img,google,id,apellido,telefono,calificacion,perfil_linkedlin,resena,documents,documento_identidad,categoria,materia,titulo_prof,numero_nequi,numero_daviplata,banco,numero_cuenta,tipo_cuenta,precio_hora,tipo_identificacion);
                this.utilService.saveInLocalStorage('userConnect', this.Profesor);
                this.utilService.setUserConnect(this.Profesor);
              } else {
                const {estado_perfil,nombre,email,password,role,img,google,id,apellido,telefono,calificacion,perfil_linkedlin,resena} = resp.usuario
                this.Estudiante = new Estudiante(estado_perfil,nombre,email,password,role,img,google,id,apellido,telefono,calificacion,perfil_linkedlin,resena);
                this.utilService.saveInLocalStorage('userConnect', this.Estudiante);
                this.utilService.setUserConnect(this.Estudiante);
              }
              this.utilService.saveInLocalStorage('userRole',rol.toLowerCase());
            })
          )
  }

  logout() {
    this.utilService.clearLocalStorage();
    this.router.navigateByUrl('/login');
  }

  /**
   * Metodo para actualizar los estudiantes
   */

  actualizarPerfilEst(data:{ email: string, nombre: string, apellido?: string, telefono?: string, perfil_linkedlin?: string, resena?: string}) {
    return this.http.put(`${base_url}/usuarios/estudiante/${this.uid}`,data);
  }

  /**
   * Metodo para actualizar los profesores
   */

    actualizarPerfilProf(data:{
      estado_perfil: string,
      nombre: string,
      email: string,
      password: string,
      role: boolean,
      img?: string,
      google?: string,
      id?: string,
      apellido?: string,
      telefono?: string,
      calificacion?: string,
      perfil_linkedlin?: string,
      resena?: string,
      documents?: Array<any>,
      documento_identidad?: string,
      categoria?: string,
      materia?: string,
      titulo_prof?: string,
      numero_nequi?: string,
      numero_daviplata?: string,
      banco?: string,
      numero_cuenta?: string,
      tipo_cuenta?: string,
      precio_hora?: number,
      tipo_identificacion?: string
    }) {
      console.log("Data act prof")
      console.log(data);
    return this.http.put(`${base_url}/usuarios/profesor/${this.uidprof}`,data);
  }

  /**
   * Metodo para obtener los profesores
   */

  getProfesores(): any {
    return this.http.get(`${base_url}/usuarios/profesores`);
  }

}
