import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Product } from '../../Interfaces/product';
import { UtilService } from '../util.service';

const base_url = environment.base_url;

@Injectable({
  providedIn: 'root'
})
export class ClaseService {

  constructor(private http: HttpClient, private utilService: UtilService) { }

  getNextClasses() {
    return this.http.get<any>('assets/data/proximas-clases.json')
      .toPromise()
      .then(res => res.data as any[])
      .then(data => data);
  }

  //Clases desde productos
  getClasses() {
    return this.http.get<any>('assets/data/profesores.json')
    .toPromise()
    .then(res => res.data as Product[])
    .then(data => data);
  }

  //Clases desde productos
  getGroupClasses() {
    return this.http.get<any>('assets/data/clasesGrupales.json')
    .toPromise()
    .then(res => res.data as Product[])
    .then(data => data);
  }


  getProfesores(): any {
    const token = this.utilService.getLocalStorage('token') || '';
    return this.http.get<any>(`${base_url}/usuarios/profesores`,{
      headers: {
        'x-token': token
      }
    });
  }

  // obtenerHeader(): HttpHeaders {
  //   return new HttpHeaders({
  //   });
  // }

  

}
