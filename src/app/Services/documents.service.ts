import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import * as formData from 'form-data';

const base_url = environment.base_url;

@Injectable({
  providedIn: 'root'
})

export class DocumentsService {

  constructor(private http: HttpClient) { }

  cargarDocumento(file: formData) {

    return this.http.post(`${base_url}/file/upload`,file);
  }
}
