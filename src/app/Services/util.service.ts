import { Injectable } from '@angular/core';
import Swal from 'sweetalert2';


@Injectable({
  providedIn: 'root'
})
export class UtilService {

  userRole: any;
  userConnect: any;

  constructor() { }

  /**
   * Metodo gérico para guardar valores en el localStorage
   */
  saveInLocalStorage(key: string, value: any) {
    localStorage.setItem(key, value);
  }

  /**
   * Metodo gérico para obtejer un valor del localStorage
   */

  getLocalStorage(key: string): any {
    return localStorage.getItem(key);
  }

  /**
   * Metodo gérico para obtejer eliminar cache del localStorage
   */

  clearLocalStorage() {
    localStorage.clear();
  }

  getUserRole() {
    return this.getLocalStorage('userRole');
  }

  getUserConnect() {
    return this.getLocalStorage('userConnect');
  }

  setUserConnect(user: any) {
    this.userConnect = user;
  }

  getUserConnectProfile() {
    return this.userConnect;
  }

  addCart(item: any) {
    console.log(item);

    //localStorage.setItem('uno', 'golasd');

    if(this.getLocalStorage("Class")){

      let arrayList = JSON.parse(this.getLocalStorage("Class"));

      // let count = 0;
      // let index: any;

      // for(const i in arrayList){
      //   console.log("Arraylist", arrayList[i].clase);
      //   console.log("item.class", item.clase);
        
      //   if(arrayList[i].clase == item.clase){
      //     count --;
      //     index = i;
      //   }else{
      //     count ++;
      //   }
      // }

      // if(count === arrayList.length){
      //   arrayList.push(item);
      //   console.log("arrayList.length", arrayList.length);
        
      //   console.log("count", count);

      // }
      // else{
      //   console.log("arrayList.length", arrayList.length);
        
      //   console.log("count", count);
        
      // }
      
     
      arrayList.push(item);
      this.saveInLocalStorage("Class", JSON.stringify(arrayList));

    }else{
      let arrayList = [];
      arrayList.push(item);
      console.log("entramos");
      
      this.saveInLocalStorage("Class", JSON.stringify(arrayList));
    }

   
  }

  extractCart() {
    if (this.getLocalStorage("Class")) {
      return JSON.parse(this.getLocalStorage("Class"));
    }else{
      console.log("error");
      
    }
  }

  eliminarCartItem(id: number){
    if (this.getLocalStorage("Class")) {
      let arrayList = JSON.parse(this.getLocalStorage("Class"));
      for (const i of arrayList) {
        if(i.claseDisp.id_clase === id){
          const indice = arrayList.indexOf(i);
          arrayList.splice(indice, 1);
          this.saveInLocalStorage("Class", JSON.stringify(arrayList));
          Swal.fire({
            position: 'center',
            icon: 'success',
            title: 'Proceso realizado correctamente',
            showConfirmButton: false,
            timer: 2000
          });
          window.location.reload();
        }
      }
    }else{
      console.log("error");
      
    }
  }


}
