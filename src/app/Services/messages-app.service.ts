import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MessagesAppService {

  messageData: any;
  configData: any;

  constructor(private http: HttpClient) { }


  async loadAppMessages() {
    await this.http
      .get<any>('./assets/config/copies.json')
      .toPromise()
      .then((res: any) => {
        this.messageData = res['data'].copies;
      })
      .catch((err: Error) => {
        return err;
      })
  }
}
