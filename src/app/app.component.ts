import { Component } from '@angular/core';
import {PrimeNGConfig} from 'primeng/api';
import { MessagesAppService } from './Services/messages-app.service';
import { ParametersService } from './Services/parameters/parameters.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(private messageService: MessagesAppService, private parameterService: ParametersService) { 
    this.messageService.loadAppMessages();
    this.parameterService.loadParameters();
  }

  ngOnInit() {
  }

}
