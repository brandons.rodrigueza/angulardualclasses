import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA, APP_INITIALIZER, DEFAULT_CURRENCY_CODE } from '@angular/core';

import {FormsModule} from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

// modulos generales
import { RouterModule } from '@angular/router';
import {AppRoutingModule} from './app-routing.module';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms'; 
import {FileUploadModule} from 'primeng/fileupload';

// importaciones PrimeNg
import {CalendarModule} from 'primeng/calendar';
import {InputTextModule} from 'primeng/inputtext';
import {FieldsetModule} from 'primeng/fieldset';
import {DialogModule} from 'primeng/dialog';
import {FullCalendarModule} from '@fullcalendar/angular';
import {PasswordModule} from 'primeng/password';
import {CardModule} from 'primeng/card';
import {TabMenuModule} from 'primeng/tabmenu';
import {MenuModule} from 'primeng/menu';
import {MenubarModule} from 'primeng/menubar';
import {DropdownModule} from 'primeng/dropdown';
import {AvatarModule} from 'primeng/avatar';
import {AvatarGroupModule} from 'primeng/avatargroup';
import {InputMaskModule} from 'primeng/inputmask';
import {ButtonModule} from 'primeng/button';
import {RatingModule} from 'primeng/rating';
import {InputTextareaModule} from 'primeng/inputtextarea';
import {MessagesModule} from 'primeng/messages';
import {MessageModule} from 'primeng/message';
import {TableModule} from 'primeng/table';

import { AppComponent } from './app.component';


import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import interactionPlugin from '@fullcalendar/interaction';
import { LoginComponent } from './Auth/login/login.component';
import { RegistroComponent } from './Auth/registro/registro.component';
import { DashboardComponent } from './Pages/dashboard/dashboard.component';
import { TopbarComponent } from './Pages/topbar/topbar.component';
import { CommonModule, DatePipe, registerLocaleData } from '@angular/common';
import { ClassroomComponent } from './Pages/classroom/classroom.component';
import { ProfileComponent } from './Pages/profile/profile.component';
import {CarouselModule} from 'primeng/carousel';
import { MainComponent } from './Pages/main.component';
import { EventService } from './Services/event.service';
import { ProductService } from './Services/productservice';
import { PhotoService } from './Services/photoservice';
import { MessagesAppService } from './Services/messages-app.service';
import { CalendarComponent } from './Pages/calendar/calendar.component';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import {DividerModule} from 'primeng/divider';
import localEs from '@angular/common/locales/es'
import { TabViewModule } from 'primeng/tabview';
import { TimelineModule } from 'primeng/timeline';
import { ClassComponent } from './Pages/class/class.component';
import { DataViewModule } from 'primeng/dataview';
import { PanelModule } from 'primeng/panel';
import { RippleModule } from 'primeng/ripple';
import { AdminComponent } from './Pages/admin/admin.component';
import { ListEstAdminComponent } from './Pages/list-est-admin/list-est-admin.component';
import { ListProfAdminComponent } from './Pages/list-prof-admin/list-prof-admin.component';
import { CarroComprasComponent } from './Pages/carro-compras/carro-compras.component';


registerLocaleData(localEs, 'es')

export function createTranslateLoader (http: HttpClient): TranslateHttpLoader {
  return new TranslateHttpLoader (http, './assets/i18n/', '.json');
}

export function loadResources (
  messagesAppService: MessagesAppService
) {
  return async () => {
    await messagesAppService.loadAppMessages();
  }
}

FullCalendarModule.registerPlugins([
    dayGridPlugin,
    timeGridPlugin,
    interactionPlugin
]);

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegistroComponent,
    DashboardComponent,
    TopbarComponent,
    ClassroomComponent,
    ProfileComponent,
    MainComponent,
    CalendarComponent,
    ClassComponent,
    AdminComponent,
    ListEstAdminComponent,
    ListProfAdminComponent,
    CarroComprasComponent
  ],
  imports: [
    CommonModule,
    TranslateModule.forRoot({
      defaultLanguage: 'es',
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
    FormsModule,
    RouterModule,
    AppRoutingModule,
    BrowserModule,
    CalendarModule,
    InputTextModule,
    FieldsetModule,
    BrowserAnimationsModule,
    DialogModule,
    FullCalendarModule,
    PasswordModule,
    ReactiveFormsModule,
    HttpClientModule,
    CardModule,
    TabMenuModule,
    MenuModule,
    MenubarModule,
    CarouselModule,
    DropdownModule,
    AvatarModule,
    AvatarGroupModule,
    InputMaskModule,
    ButtonModule,
    RatingModule,
    InputTextareaModule,
    FileUploadModule,
    MessagesModule,
    MessageModule,
    DividerModule,
    TabViewModule,
    TimelineModule,
    DataViewModule,
    PanelModule,
    RippleModule,
    TableModule
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: loadResources,
      multi: true,
      deps: [MessagesAppService]
    },
    {provide: DEFAULT_CURRENCY_CODE, useValue: 'USD'},
    EventService, DatePipe, ProductService, PhotoService
  ],
  bootstrap: [AppComponent],
  schemas: [ 
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA 
  ]
})
export class AppModule { }
