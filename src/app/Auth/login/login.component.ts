import { Component, NgZone, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { UsuarioService } from 'src/app/Services/usuario.service';
import Swal from 'sweetalert2'
import { Router } from '@angular/router';
import { MessagesAppService } from 'src/app/Services/messages-app.service';

declare const gapi: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  formSubmitted: boolean = false;
  displayPosition: boolean = false;
  position: string = '';

  public auth2: any;

  public loginForm = this.fb.group({
    email: ['' ,Validators.required],
    password: ['',Validators.required],
  });

  constructor(private fb: FormBuilder, private usuarioService: UsuarioService, private router: Router, private zone: NgZone, public messagesAppService: MessagesAppService) { }

  ngOnInit(): void {
    this.renderButton();
  }

  login() {
    this.formSubmitted = true;
    this.usuarioService.login(this.loginForm.value)
    .subscribe( {
      error: (e) => {
        Swal.fire('Error', e.error.msg, 'error');
      },
      complete: () =>{
        Swal.fire({
          position: 'top-end',
          icon: 'success',
          title: 'Ingreso correcto',
          showConfirmButton: false,
          timer: 1500
        })
        this.router.navigateByUrl('/dashboard');
      }
    })
  }

  campoNoValido(campo: string): boolean {
    if (this.loginForm.get(campo)?.invalid && this.formSubmitted) {
      return true;
    } else {
      return false;
    }
  }

  showPositionDialog(position: string) {
    this.position = position;
    this.displayPosition = true;
  }

  renderButton() {
    gapi.signin2.render('my-signin2', {
      'scope': 'profile email',
      'width': 240,
      'height': 50,
      'longtitle': true,
      'theme': 'dark'
    });
    this.startApp();
  }

  startApp() {
    gapi.load('auth2', () => {
      // Retrieve the singleton for the GoogleAuth library and set up the client.
      this.auth2 = gapi.auth2.init({
        client_id: '336367600990-on5nil499flo5j82tcfkptl1bpjq5rfv.apps.googleusercontent.com',
        cookiepolicy: 'single_host_origin',
      });
      this.attachSignin(document.getElementById('my-signin2'));
    });
  }

  attachSignin(element: HTMLElement | null) {
    this.auth2.attachClickHandler(element, {},
        (googleUser: { getAuthResponse: () => { (): any; new(): any; id_token: any; }; }) => {
            const id_token = googleUser.getAuthResponse().id_token;
            this.usuarioService.loginGoogle(id_token)
              .subscribe((resp: any) => {
                this.zone.run(() => {
                  this.router.navigateByUrl('/dashboard');     
                });
            });
        }, (error: any) =>{
          console.log(error);
        });
  }

}
