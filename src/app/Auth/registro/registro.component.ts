import { Component, NgZone, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { registroForm } from 'src/app/Interfaces/registro-form.interface';
import { ParametersService } from 'src/app/Services/parameters/parameters.service';
import { UsuarioService } from 'src/app/Services/usuario.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.scss']
})
export class RegistroComponent implements OnInit {

  formSubmitted: boolean = false;
  position: string = '';
  roles: any;
  displayModal: boolean = false;
  registroFormInterface: registroForm = {email:'email',nombre:'nombre',password:'pass',role:'role',estado_perfil:'initial_status'};

  public auth2: any;

  public registroForm = this.fb.group({
    email: ['' ,Validators.required],
    password: ['',Validators.required],
    nombre: ['', Validators.required],
    role: [null,Validators.required]
  });

  constructor(private fb: FormBuilder, private usuarioService: UsuarioService, private router: Router, private zone: NgZone, private parameterService: ParametersService) { }

  ngOnInit(): void {
    this.roles = [
      {name: 'Estudiante', code: 'Estudiante'},
      {name: 'Profesor', code: 'Profesor'},
    ]
  }

  changeMe(value: any) {
    this.registroForm.value.role = this.registroForm.value.role.code;
    if(value === 'Profesor') {
      this.showModalDialog()    
    }
  }

  showModalDialog() {
    this.displayModal = true;
  }

  registro() {
    this.formSubmitted = true;
    this.registroFormInterface = this.registroForm.value;
    if(this.registroFormInterface.role === 'Profesor') {
      this.registroFormInterface.estado_perfil = this.parameterService.parametersData.profile.inactive_status;
    } else {
      this.registroFormInterface.estado_perfil = this.parameterService.parametersData.profile.active_status;
    }
    this.usuarioService.crearUsuario(this.registroFormInterface)
      .subscribe( {
        next: (v) => console.log(v),
        error: (e) => {
          Swal.fire('Error', e.error.msg, 'error');
        },
        complete: () =>{
          Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Registro correcto',
            text: 'Ya puedes iniciar sesión con tu correo',
            showConfirmButton: false,
            timer: 1500
          })
          this.router.navigateByUrl('/login');
        }
  })
}

  campoNoValido(campo: string): boolean {
    if ((this.registroForm.get(campo)?.invalid && this.formSubmitted) || (this.registroForm.get(campo) === null && this.formSubmitted)) {
      return true;
    } else {
      return false;
    }
  }

}