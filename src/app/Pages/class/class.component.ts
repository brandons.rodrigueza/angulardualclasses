import { DatePipe } from '@angular/common';
import { Component, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { SelectItem } from 'primeng/api';
import { takeUntil, Subject } from 'rxjs';
import { Clase } from 'src/app/Interfaces/clase.interface';
import { Product } from 'src/app/Interfaces/product';
import { UtilService } from 'src/app/Services/util.service';
import Swal from 'sweetalert2';
import { ClaseService } from '../../Services/clases/clase.service';

@Component({
  selector: 'app-class',
  templateUrl: './class.component.html',
  styleUrls: ['./class.component.scss']
})
export class ClassComponent implements OnInit {

  claseOfertaGeneral: Product[] = [];

  clasePrueba: any = [];
  clase: any;

  sortOptions: SelectItem[] = [];

  sortOptionsGrupal: SelectItem[] = [];
  
  sortKey: any;

  sortOrder: number = 0;

  sortField: string = '';

  clasesGrupales: Product[] = [];

  displayModal: boolean;

  nombre: string;

  clasesDisponibles: Clase [] = [];

  horaClase: string = '2022-05-07T13:30:00';

  datePipe = new DatePipe(this.translate.getDefaultLang());

  unsubscribe$ = new Subject<void>();

  constructor(private claseService: ClaseService,
              private translate: TranslateService,
              private router: Router,
              private utilService: UtilService) { }

  ngOnInit() {
    //this.claseService.getClasses().then(data => this.claseOfertaGeneral = data);

    this.claseService.getGroupClasses().then(data => this.clasesGrupales = data);

    this.sortOptions = [
      { label: 'Precio mayor a menor', value: '!precio_hora' },
      { label: 'Precio menor a mayor', value: 'precio_hora' }
    ];

    this.sortOptionsGrupal = [
      { label: 'Precio mayor a menor', value: '!precio_clase' },
      { label: 'Precio menor a mayor', value: 'precio_clase' }
    ];

    this.inicializarProximosEventos();
    this.listarProfesores();
  }

  onSortChange(event: any) {
    let value = event.value;

    if (value.indexOf('!') === 0) {
      this.sortOrder = -1;
      this.sortField = value.substring(1, value.length);
    }
    else {
      this.sortOrder = 1;
      this.sortField = value;
    }
  }

  showModalDialog(clase: Product) {
    this.clase = {...clase}
    //this.clase.nombre = this.nombre
    console.log(this.clase)
    this.displayModal = true;
  }

  seleccionHorario(claseDisp: any){
    const item = {
      claseDisp: claseDisp,
    }

    this.utilService.addCart(item);
  }

  //POSIBLE METODO GENERAL
  inicializarProximosEventos(){
    this.claseService.getNextClasses().then((clases: any[]) => {
        this.clasesDisponibles = clases;
        console.log(clases);
        
        for (let i = 0; i < this.clasesDisponibles.length; i++) {
            this.clasesDisponibles[i].dia_clase =  this.organizarFecha(this.clasesDisponibles[i].hora_inicio);
            this.clasesDisponibles[i].hora_inicio =  this.organizarHora(this.clasesDisponibles[i].hora_inicio);
            this.clasesDisponibles[i].hora_fin =  this.organizarHora(this.clasesDisponibles[i].hora_fin);
        }
    });
  }

  listarProfesores(){
    this.claseService.getProfesores()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (data: any) => {
        this.claseOfertaGeneral = data.profesores;
      },
      (error: any) => {
        Swal.fire('Error', error.error.msg, 'error');
      }
      );
    console.log("Clase general", this.claseOfertaGeneral);
  }

  //POSIBLE METODO GENERAL
  organizarFecha(fecha: any) {
    // console.log(fecha);
    return this.datePipe.transform(fecha, 'EEEE, dd \'de\' MMMM');
  }

  organizarHora(fecha: any) {
    // console.log(fecha);
    return this.datePipe.transform(fecha, ' hh:mm aa');
  }

}
