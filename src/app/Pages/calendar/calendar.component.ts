import { Component, OnInit } from '@angular/core';
import { EventService } from 'src/app/Services/event.service';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent implements OnInit {

  options: any;

  events: any[] = [];

  date: Date = new Date();

  eventDialog: boolean = false;

  changedEvent: any;

  clickedEvent: any = null;

  constructor(private eventService: EventService) { }

  ngOnInit(): void {

    this.eventService.getEvents().then((events: any[]) => {
            this.events = events;
            this.options = {...this.options, ...{events: events}};
        });

    this.options = {
            initialDate: this.date,
            headerToolbar: {
                left: '',
                center: 'title',
                right: 'prev,next',
                //left: 'prev,next today',
                //right: 'dayGridMonth,timeGridWeek,timeGridDay'
            },
            editable: true,
            selectable: true,
            selectMirror: true,
            dayMaxEvents: true,
            eventClick: (e: { event: any; }) => {
                this.eventDialog = true;

                this.clickedEvent = e.event;

                this.changedEvent.title = this.clickedEvent.title;
                this.changedEvent.start = this.clickedEvent.start;
                this.changedEvent.end = this.clickedEvent.end;
            }
        };

        this.changedEvent = {title: '', start: null, end: '', allDay: null};
  }
  save() {
        this.eventDialog = false;

        this.clickedEvent.setProp('title', this.changedEvent.title);
        this.clickedEvent.setStart(this.changedEvent.start);
        this.clickedEvent.setEnd(this.changedEvent.end);
        this.clickedEvent.setAllDay(this.changedEvent.allDay);

        this.changedEvent = { title: '', start: null, end: '', allDay: null };
    }

    reset() {
        this.changedEvent.title = this.clickedEvent.title;
        this.changedEvent.start = this.clickedEvent.start;
        this.changedEvent.end = this.clickedEvent.end;
    }
}
