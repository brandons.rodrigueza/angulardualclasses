import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListEstAdminComponent } from './list-est-admin.component';

describe('ListEstAdminComponent', () => {
  let component: ListEstAdminComponent;
  let fixture: ComponentFixture<ListEstAdminComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListEstAdminComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListEstAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
