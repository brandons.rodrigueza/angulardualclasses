import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { Router } from '@angular/router';
import { UsuarioService } from 'src/app/Services/usuario.service';

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.scss']
})
export class TopbarComponent implements OnInit {

  items: MenuItem[] = [];
  options: MenuItem[] = [];

  activeTopbarItem: any;
  topbarItemClick: any;
  
  constructor(private router: Router, private usuarioService: UsuarioService) { }

  ngOnInit(): void {
      this.items = [
        {
          label: 'Inicio',
          icon: 'pi pi-home',
          command: () => {
            this.onTopbarItemClick('dashboard');
        }
        },
        {
            label: 'Clases',
            icon: 'pi pi-palette',
            items: [
              {
                  label: 'Agendar clase',
                  icon: 'pi pi-book',
                  command: () => {
                    this.onTopbarItemClick('classroom');
                }
              },
              {
                    label: 'Clases programadas', 
                    icon: 'pi pi-calendar',
                    // items: [
                    //     {label: 'Project'},
                    //     {label: 'Other'},
                    // ]
                // },
                // {label: 'Open'},
                // {label: 'Quit'}
            }
            ]
        },
        {
            label: 'Aula virtual',
            icon: 'pi pi-building'
        },
        {
            label: 'Estadisticas',
            icon: 'pi pi-chart-line',
        },
        {
          label: 'Control Administrador',
          icon: 'pi pi-shield',
          command: () => {
            this.onTopbarItemClick('admin');
        }
        },
    ];
    this.options = [
      {
        label: 'Perfil',
        icon: 'pi pi-user',
        command: () => {
          this.onTopbarItemClick('profile');
      }

      },
      {
        label: 'Cambio de contraseña',
        icon: 'pi pi-ellipsis-h'
      },
      {
        label: 'Cerrar sesión',
        icon: 'pi pi-sign-out',
        command: () => {
          this.closeSession();
        }
      }
    ]
  }

  onTopbarItemClick(URL: string): void {
    this.router.navigate([URL]); 
    console.log("evento");
  }

  closeSession() {
    this.usuarioService.logout();
    console.log('cerre sesion');
  }

  abrirCarrito() {
    this.router.navigate(['/carro_compras'])
  }


}
