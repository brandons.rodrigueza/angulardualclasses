import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListProfAdminComponent } from './list-prof-admin.component';

describe('ListProfAdminComponent', () => {
  let component: ListProfAdminComponent;
  let fixture: ComponentFixture<ListProfAdminComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListProfAdminComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListProfAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
