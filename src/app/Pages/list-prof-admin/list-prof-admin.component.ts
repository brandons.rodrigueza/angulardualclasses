import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Profesor } from 'src/app/Models/profesor.model';
import { UsuarioService } from 'src/app/Services/usuario.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-list-prof-admin',
  templateUrl: './list-prof-admin.component.html',
  styleUrls: ['./list-prof-admin.component.scss']
})
export class ListProfAdminComponent implements OnInit {

  public editFormProfesorStatus: FormGroup;
  profesorDialog: boolean;
  profesores: Profesor[];
  profesor: Profesor;
  statusprof: any;
  submitted: boolean;

  constructor(private usuarioService: UsuarioService, private fb: FormBuilder) { }

  ngOnInit(): void {

    this.editFormProfesorStatus = this.fb.group({
      estado_perfil: ['']
    })

    this.usuarioService.getProfesores()
    .subscribe({
      next: (v:any) => {
        this.profesores = v.profesores;
      },
      error: (e:any) => {
        Swal.fire('Error', e.error.msg, 'error');
      }
    });
    this.statusprof = [
      {name: 'ACTIVO', code: 'Activo'},
      {name: 'INACTIVO', code: 'Inactivo'},
      {name: 'DENEGADO', code: 'Denegado'}
  ];
  }

  editProfesor(profesor: Profesor) {
    this.profesor = {...profesor};
    this.profesorDialog = true;
  }

  hideDialog() {
    this.profesorDialog = false;
    this.submitted = false;
  }

  EditStatusProfesor() {
    this.profesor.estado_perfil = this.statusprof.code;
    console.log("pruebaaaa");
    console.log(this.profesor.id);
    console.log(this.profesor.estado_perfil);
  }

}
