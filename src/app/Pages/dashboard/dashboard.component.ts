import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/Interfaces/product';
import { EventService } from 'src/app/Services/event.service';
import { ProductService } from 'src/app/Services/productservice';
import { PhotoService } from 'src/app/Services/photoservice';
import { ClaseService } from '../../Services/clases/clase.service';
import { Clase } from '../../Interfaces/clase.interface';
import { TranslateService } from '@ngx-translate/core';
import {PrimeIcons} from 'primeng/api';
import { UtilService } from 'src/app/Services/util.service';
import { takeUntil } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { Subject } from 'rxjs';


@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

    clases: Clase[] = [];

    clasesGrupales: Product[] = [];

    currentDate?: any;

    currentDateString: string | null = null;

    events: any[] = [];

    options: any;

    header: any;

    eventDialog: boolean = false;

    changedEvent: any;

    clickedEvent: any = null;

    todayDate: Date = new Date();

    topProfesores: any[] = [];

    images: any[] = [];

    datePipe = new DatePipe(this.translate.getDefaultLang());

    customEvents!: any[];
    
    userConnect: any;

    habilitar: boolean = false;

    publicacionString: string = '';

    displayModal: boolean;

    unsubscribe$ = new Subject<void>();

    galleriaResponsiveOptions: any[] = [
        {
            breakpoint: '1024px',
            numVisible: 5
        },
        {
            breakpoint: '960px',
            numVisible: 4
        },
        {
            breakpoint: '768px',
            numVisible: 3
        },
        {
            breakpoint: '560px',
            numVisible: 1
        }
    ];

    carouselResponsiveOptions: any[] = [
        {
            breakpoint: '1024px',
            numVisible: 3,
            numScroll: 3
        },
        {
            breakpoint: '768px',
            numVisible: 2,
            numScroll: 2
        },
        {
            breakpoint: '560px',
            numVisible: 1,
            numScroll: 1
        }
    ];

    constructor(private eventService: EventService,
        private claseService: ClaseService,
        private translate: TranslateService, 
        private photoService: PhotoService,
        private utilService: UtilService) {
    }

    ngOnInit() {
        this.inicializarProximosEventos();
        this.customEvents = [
            {
                icon: PrimeIcons.USER,
                color: '#464DF2',
                texto: 'Aqui Encontrarás la manera más rápida y sencilla de solucionar tus dudas acerca de cualquier tema.'
            },{
                icon: PrimeIcons.SEARCH_PLUS,
                color: '#464DF2',
                texto: 'Puedes escojer entre profesores o clases programadas para grupos de más de 2 personas al mismo tiempo.'
            }, {
                icon: PrimeIcons.TABLE,
                color: '#464DF2',
                texto: 'Mediante reuniones en vivo en formato tutoria y desde cualquier lugar en el que te encuentres.'
            },
            
        ];
        this.userConnect = this.utilService.getUserConnectProfile();
        this.listarProfesores();
        this.claseService.getGroupClasses().then(data => this.clasesGrupales = data);
        this.photoService.getImages().then(images => {
            this.images = images;
        });

    }

    listarProfesores(){
        this.claseService.getProfesores()
          .pipe(takeUntil(this.unsubscribe$))
          .subscribe(
            (data: any) => {
            this.topProfesores = data.profesores;
          },
          (error: any) => {
            Swal.fire('Error', error.error.msg, 'error');
          }
          );
        console.log("Clase general", this.topProfesores);
      }
    
    inicializarProximosEventos(){
        this.currentDateString = this.organizarFecha(this.todayDate);
        this.claseService.getNextClasses().then((clases: any[]) => {
            this.clases = clases;
            for (let i = 0; i < this.clases.length; i++) {
                this.clases[i].dia_clase =  this.organizarFecha(this.clases[i].hora_inicio);
                this.clases[i].hora_inicio =  this.organizarHora(this.clases[i].hora_inicio);
                this.clases[i].hora_fin =  this.organizarHora(this.clases[i].hora_fin);
            }
        });
    }

    organizarFecha(fecha: any) {
        // console.log(fecha);
        return this.datePipe.transform(fecha, 'EEEE, dd \'de\' MMMM');
    }

    organizarHora(fecha: any) {
        // console.log(fecha);
        return this.datePipe.transform(fecha, ' hh:mm aa');
    }

    publicacion(e: any) {
        this.habilitar = true;
    }

    showModalDialog() {
        this.displayModal = true;
    }
}