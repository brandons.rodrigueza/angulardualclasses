import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { UtilService } from 'src/app/Services/util.service';
import Swal from 'sweetalert2';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-carro-compras',
  templateUrl: './carro-compras.component.html',
  styleUrls: ['./carro-compras.component.scss']
})
export class CarroComprasComponent implements OnInit {

  // public carritoForm: FormGroup;
  public carroItemsList: any = [];
  public cols: { field: string; header: string; }[];

  // @Output() idEliminar = new EventEmitter<any>;

  

  constructor(private utilService: UtilService,
              private fb: FormBuilder,) { }

  ngOnInit(): void {
    this.crearFormulario();
    this.extraerInfoCarroCompras();

    // this.carritoForm = this.fb.group({
    //   seleccion: ['', Validators.required]
    // })
  }

  crearFormulario(){
    this.cols = [
      {
        field: 'id_clase',
        header: "ID Clase"
      },
      {
        field: 'titulo',
        header: "Titulo"
      },
      {
        field: 'dia_clase',
        header: "Fecha de clase"
      },
      {
        field: 'hora_inicio',
        header: "Hora inicio"
      },
      {
        field: 'hora_fin',
        header: "Hora fin"
      },
      {
        field: 'acciones',
        header: "Acciones"
      },
    ]
  }

  extraerInfoCarroCompras(){
    this.carroItemsList = this.utilService.extractCart();
    // for(let item of carroRequest){
    //   console.log("list",item);
    //   var items =  [{
    //     id_clase: item.claseDisp.id_clase,
    //     titulo: item.claseDisp.titulo,
    //     dia_clase: item.claseDisp.dia_clase,
    //     hora_inicio: item.claseDisp.hora_inicio,
    //     hora_fin: item.claseDisp.hora_fin
    //   }]
    //   this.carroItemsList.push(items)  
    // }
    console.log(this.carroItemsList);
  }


  eliminarItem($event: any){
    // this.idEliminar.emit($event.claseDisp.id_clase);
    //console.log("event", $event);
    this.utilService.eliminarCartItem($event.claseDisp.id_clase);
  }

}
