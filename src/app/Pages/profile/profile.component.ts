import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Estudiante } from 'src/app/Models/estudiante.model';
import { Profesor } from 'src/app/Models/profesor.model';
import { DocumentsService } from 'src/app/Services/documents.service';
import { UsuarioService } from 'src/app/Services/usuario.service';
import { UtilService } from 'src/app/Services/util.service';
import Swal from 'sweetalert2';
import * as formData from 'form-data';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  public editFormEstudiante: FormGroup;
  public editFormProfesor: FormGroup;
  public Estudiante: Estudiante;
  public Profesor: Profesor;

  disabled: boolean = true;
  rolEstudiante: boolean = false;
  rolProfesor: boolean = false;
  valRating: number = 5;
  msg: string = '';
  categoriaEn: any;
  bancos: any;
  tipoCuenta: any;
  tipo_identificacion: any;
  uploadedFiles: any[] = [];
  multiple: boolean = true;
  customUpload: boolean = true;
  rol: any;
  userConnectEstudiante: Estudiante;
  userConnectProfesor: Profesor;
  idUserConnect: any;
  avnButton: boolean;

  constructor(private utilService: UtilService, private fb: FormBuilder, private usuarioService: UsuarioService, private router: Router, private documentService: DocumentsService) {
    this.avnButton = false;
  }

  ngOnInit(): void {

    this.rol = this.utilService.getUserRole();

    this.editFormEstudiante = this.fb.group({
      nombre: ['', Validators.required],
      email: ['', Validators.required],
      apellido: [''],
      telefono: [''],
      perfil_linkedlin: [''],
      resena: [''],
      calificacion: []
    })

    this.editFormProfesor = this.fb.group({
      nombre: ['', Validators.required],
      apellido: [''],
      email: ['', Validators.required],
      telefono: [''],
      calificacion: [''],
      perfil_linkedlin: [''],
      resena: [''],
      categoria: [''],
      materia: [''],
      titulo_prof: [''],
      numero_nequi: [''],
      numero_daviplata: [''],
      banco: [''],
      numero_cuenta: [''],
      tipo_cuenta: [''],
      precio_hora: [''],
      tipo_identificacion: [''],
      documento_identidad: [''],
      documents: ['']
    })

    if(this.rol === 'estudiante') {
      this.rolEstudiante = true;
      this.rolProfesor = false;
      this.userConnectEstudiante = this.utilService.getUserConnectProfile();
      this.idUserConnect = this.userConnectEstudiante.id;
    } else {
      this.rolEstudiante = false;
      this.rolProfesor = true;
      this.userConnectProfesor = this.utilService.getUserConnectProfile();
      console.log(this.userConnectProfesor);
      this.idUserConnect = this.userConnectProfesor.id;
      console.log("tipo id");
      console.log(this.userConnectProfesor.tipo_identificacion);
    }

    this.categoriaEn = [
      {name: 'Tecnología', code: 'Tecnología'},
      {name: 'Salud', code: 'Tecnología'},
      {name: 'Educación Primaria', code: 'Educación Primaria'},
      {name: 'Educación Bachillerato', code: 'Educación Bachillerato'},
      {name: 'Educación Universitaria', code: 'Educación Universitaria'},
    ];
    this.bancos = [
      {name: 'Bancolombia', code: 'Bancolombia'},
      {name: 'Banco de Bogota', code: 'Banco de Bogota'},
      {name: 'Davivienda', code: 'Davivienda'},
    ];
    this.tipoCuenta = [
      {name: 'Ahorros', code: 'Ahorros'},
      {name: 'Corriente', code: 'Corriente'},
    ];
    this.tipo_identificacion = [
      {name: 'Cedula de ciudadania', code: 'Cedula de ciudadania'},
      {name: 'Pasaporte', code: 'Pasaporte'},
    ];
  }

  onUpload(event:any) {
    for(let file of event.files) {
        this.uploadedFiles.push(file);
        this.uploadedFiles.forEach(element => {
          console.log(element.name);
          console.log(this.idUserConnect);
        });
    }
    Swal.fire({
      position: 'top-end',
      icon: 'success',
      title: 'Documentos cargados correctamente',
      showConfirmButton: false,
      timer: 1500
    })
    this.avnButton = true;
  }


  EditEstudiante() {
    console.log(this.editFormEstudiante.value);
    this.usuarioService.actualizarPerfilEst(this.editFormEstudiante.value)
    .subscribe( {
      next: (v) => console.log(v),
      error: (e) => {
        Swal.fire('Error', e.error.msg, 'error');
      },
      complete: () =>{
        Swal.fire({
          position: 'top-end',
          icon: 'success',
          title: 'Actualización de datos correcta',
          text: 'Haz actualizado tu información, ahora es pública para tus seguidores',
          showConfirmButton: false,
          timer: 1500
        })
        this.router.navigateByUrl('/dashboard');
      }
    })
  }

  EditProfesor() {
    this.userConnectProfesor.documents = this.uploadedFiles;
    this.userConnectProfesor.categoria = this.editFormProfesor.value.categoria.code;
    this.userConnectProfesor.banco = this.editFormProfesor.value.banco.code;
    this.userConnectProfesor.tipo_identificacion = this.editFormProfesor.value.tipo_identificacion.code;
    this.userConnectProfesor.tipo_cuenta = this.editFormProfesor.value.tipo_cuenta.code;
    this.usuarioService.actualizarPerfilProf(this.userConnectProfesor)
      .subscribe( {
        next: (v) => console.log(v),
        error: (e) => {
          Swal.fire('Error', e.error.msg, 'error');
        },
        complete: () =>{
          Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Actualización de datos correcta',
            text: 'Haz actualizado tu información, ahora es pública para tus seguidores',
            showConfirmButton: false,
            timer: 1500
          })
          this.router.navigateByUrl('/dashboard');
        }
    })

    //  const form = new formData();
    //  form.append('file', this.uploadedFiles[0]);
    //  form.append('id', this.idUserConnect);
    //  this.documentService.cargarDocumento(form)
    //  .subscribe( {
    //    next: (v) => console.log(v),
    //    error: (e) => {
    //      Swal.fire('Error', e.error.msg, 'error');
    //    }
    //  });
  }
  

}
